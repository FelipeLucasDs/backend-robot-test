package com.felipelucas.robot.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class Position implements Cloneable{

    private int x;
    private int y;
    private VisionSide visionSide;

    public boolean positionValid(Integer boardX, Integer boardY){
        return x >= 0 && x < boardX && y >= 0 && y < boardY;
    }

    protected Position clone() {
        try {
            return (Position) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
}
