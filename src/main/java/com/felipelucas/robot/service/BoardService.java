package com.felipelucas.robot.service;

import com.felipelucas.commons.exceptions.OutOfBoundsException;
import com.felipelucas.robot.domain.CommandStep;
import com.felipelucas.robot.domain.Position;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BoardService {

    @Value("${board.x}" )
    private Integer boardX;

    @Value("${board.y}" )
    private Integer boardY;

    public void positionIsValid(Position position, CommandStep commandStep) throws OutOfBoundsException{
        if(!position.positionValid(boardX, boardY)){
            throw new OutOfBoundsException(commandStep);
        }

    }


}
